delimiter //
create procedure query3()

begin
create temporary table mytbl1
select date_format(dates, "%Y-%m-%d") 'date', max(close_price - open_price) as 'range'
from dataset3
group by date_format(dates, "%Y-%m-%d")
order by max(close_price - open_price) desc
limit 3;


create temporary table mytbl2
select date_format(dates, "%Y-%m-%d") 'date', max(high_price) as 'MaxHigh'
from dataset3
where date_format(dates, "%Y-%m-%d") 
in(select date_format(date, "%Y-%m-%d") 'date' from mytbl1)
group by date_format(dates, "%Y-%m-%d");


create temporary table mytbl3
select date_format(dates, "%Y-%m-%d") 'date', date_format((dates), "%H:%i") as 'time'
from dataset3, mytbl2
where date_format(dataset3.dates, "%Y-%m-%d") = date_format(mytbl2.date, "%Y-%m-%d") 
and MaxHigh = high_price
group by date_format(dates, "%Y-%m-%d");


select mytbl1.date, mytbl1.range, mytbl2.maxhigh, mytbl3.time from mytbl1
		inner join mytbl2 on mytbl2.date = mytbl1.date
        inner join mytbl3 on mytbl3.date = mytbl1.date
        order by mytbl1.range;

drop temporary table mytbl1;
drop temporary table mytbl2;
drop temporary table mytbl3;


end // 
delimiter ;

call query3();


 
    