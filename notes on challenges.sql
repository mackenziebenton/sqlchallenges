challenge 2 notes

SELECT 
sum(volume*close_price)/sum(volume) as VOLUME_WEIGHTED_PRICE, 
date_format(dates, "%d/%m/%Y"), time_format(dates, "%H%i") 
FROM dataset2 where dates between '2010-10-11 09:00:00' AND '2010-10-11 14:00:00'
and ticker = 'AAPL'
group by date_format(dates, "%d/%m/%Y");

use mackenzie;

SELECT * FROM mackenzie.dataset2;

SELECT TRADING_SYMBOL,
SUM(TRADE_SIZE*TRADE_PRICE)/SUM(TRADE_SIZE) as VOLUME_WEIGHTED_PRICE
FROM STOCK_TRADE
WHERE TRADE_TIME BETWEEN '2005-11-14 12:00' AND '2005-11-14 15:00'
AND TRADING_SYMBOL = 'ADV'
GROUP BY TRADING_SYMBOL

delimiter //
create procedure query2(IN in_date date)
begin
	select ticker 'ticker', sum(volume*close_price)/sum(volume) as 'volume_weighted_price'
    from dataset2 where dates = in_date
    order by sum(volume*close_price)/sum(volume)
    where dates between '2010-10-11 7:00:00' AND '2010-10-11 12:00:00'
    and ticker = 'test'
    group by ticker;

end // 
delimiter ;

call query2(20101011);



call query2(20101011);


SET Global sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY',''));




