use mackenzie; 


create table dataset1 (ticker CHAR(9) primary key,
						dates date,
                        open_price double(10,2),
                        high_price double(10,2),
                        low_price double (10,2),
                        close_price double(10,2),
                        volume int(10));
 
					
create table dataset2 (ticker CHAR(10),
						dates VARCHAR(30), 
						open_price double (10,6),
                        high_price double (10,6),
                        low_price double (10,6),
                        close_price double (10,6),
                        volume int(10));
 drop table dataset3;                      
create table dataset3 (dates datetime,
						open_price double (10,6),
                        high_price double (10,6),
                        low_price double (10,6),
                        close_price double (10,6),
                        volume int(10));
                          
                


insert into dataset1 
select * from steve.sample1;


insert into dataset2 
select ticker, str_to_date(date, "%Y%m%d%H%i"), open, high, low, close, vol
		from steve.sample2;


insert into dataset3 
	select str_to_date(concat(date,'',time), "%m/%d/%Y %H%i"),  
	open, high, low, close, volume 
from steve.sample3;






                        
                        
                        
                        
                        