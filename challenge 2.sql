delimiter //
create procedure vwap(IN start_date datetime) 
begin 
declare end_date datetime;
set end_date = date_add(start_date,interval 5 hour);
select start_date;
SELECT 
sum(volume*close_price)/sum(volume) as VOLUME_WEIGHTED_PRICE, 
date_format(dates, "%d/%m/%Y") as "Date", 
concat('Start: ',start_date, ' - End:',end_date) as "Time" 
FROM dataset2 where dates between start_date AND end_date
and ticker = 'AAPL'
group by date_format(dates, "%d/%m/%Y");


end // 
delimiter ;

call vwap("2010-10-11 09:00:00");

drop procedure vwap;



