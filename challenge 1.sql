use mackenzie;

SELECT * FROM mackenzie.dataset1;


delimiter //
create procedure query1(IN in_date date)
begin
	select ticker 'ticker', (((close_price - open_price)/ open_price)*100) 'percentage gain'
    from dataset1 where dates = in_date
    order by (((close_price - open_price)/ open_price)*100) DESC limit 5;

end // 
delimiter ;

call query1(20101105);








